package com.reactit.service.dto;

import java.math.BigDecimal;

public class FaceComparisonDTO {
	
	private Boolean combina;
	private BigDecimal porcentagem;
	
	public Boolean getCombina() {
		return combina;
	}
	public void setCombina(Boolean combina) {
		this.combina = combina;
	}
	public BigDecimal getPorcentagem() {
		return porcentagem;
	}
	public void setPorcentagem(BigDecimal porcentagem) {
		this.porcentagem = porcentagem;
	}	
}
