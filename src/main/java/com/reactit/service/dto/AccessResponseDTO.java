package com.reactit.service.dto;

public class AccessResponseDTO {
	
    private String uuid;
    private String captchaBase64;
    private String cookie;
    
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getCaptchaBase64() {
		return captchaBase64;
	}
	public void setCaptchaBase64(String captchaBase64) {
		this.captchaBase64 = captchaBase64;
	}
	public String getCookie() {
		return cookie;
	}
	public void setCookie(String cookie) {
		this.cookie = cookie;
	}
}
