package com.reactit.service;

import com.reactit.domain.Pessoa;
import com.reactit.repository.PessoaRepository;
import com.reactit.service.dto.AccessResponseDTO;
import com.reactit.service.dto.DocDTO;
import com.reactit.service.dto.FaceComparisonDTO;
import com.reactit.service.dto.PersonRequestDTO;
import com.reactit.service.dto.PessoaDTO;
import com.reactit.service.dto.SsnPersonDTO;
import com.reactit.service.mapper.PessoaMapper;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;

/**
 * Service Implementation for managing {@link Pessoa}.
 */
@Service
@Transactional
public class PessoaService {

    private final static Logger log = LoggerFactory.getLogger(PessoaService.class);
    
    private final static int NAME_INDEX = 1;
    private final static int BIRTH_DATE_INDEX = 2;
    private final static int STATUS_INDEX = 3;
    private final static int DATE_INSCRICAO_INDEX = 4;
    private final static int DIGITO_VERIFICADOR_INDEX = 5;

    private final PessoaRepository pessoaRepository;

    private final PessoaMapper pessoaMapper;
    
    static final int DELTA = 3;
    
    @Autowired
    private AnalyzePhotoService analyzePhoto;
    
	@Value("${ocr.aws.credential.secret-access-key:}")
	private String credentialSecretAccessKey;
	
	@Value("${ocr.aws.credential.access-key-id:}")
	private String credentialAccessKeyId;

    public PessoaService(PessoaRepository pessoaRepository, PessoaMapper pessoaMapper) {
        this.pessoaRepository = pessoaRepository;
        this.pessoaMapper = pessoaMapper;
    }

    /**
     * Save a pessoa.
     *
     * @param pessoaDTO the entity to save.
     * @return the persisted entity.
     */
    public PessoaDTO save(PessoaDTO pessoaDTO) {
        log.debug("Request to save Pessoa : {}", pessoaDTO);
        Pessoa pessoa = pessoaMapper.toEntity(pessoaDTO);
        pessoa = pessoaRepository.save(pessoa);
        return pessoaMapper.toDto(pessoa);
    }

    /**
     * Get all the pessoas.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PessoaDTO> findAll() {
        log.debug("Request to get all Pessoas");
        return pessoaRepository.findAll().stream()
            .map(pessoaMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one pessoa by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PessoaDTO> findOne(Long id) {
        log.debug("Request to get Pessoa : {}", id);
        return pessoaRepository.findById(id)
            .map(pessoaMapper::toDto);
    }

    /**
     * Delete the pessoa by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Pessoa : {}", id);
        pessoaRepository.deleteById(id);
    } 
    
    public FaceComparisonDTO getConfidenceImage(MultipartFile doc, MultipartFile target) throws FileNotFoundException, IOException {
    	return analyzePhoto.imageAnalyzeFaceComparison(doc.getBytes(), target.getBytes());
    }
    
    public DocDTO getDocsCnh(MultipartFile frente, MultipartFile verso) throws FileNotFoundException, IOException {  
    	log.info("----{} {} ---", credentialSecretAccessKey, credentialAccessKeyId);
    	
    	List<String> responseList = analyzePhoto.imageAnalyzeCnh(frente.getBytes());
    	
    	DocDTO doc = new DocDTO();
    	String[] getResponse = null;
    	String split = ": ";
    	String split2 = " ";
    	
    	for(String response : responseList) {
    		if(response.contains("Nome")) {
    			if(doc.getNome() == null || doc.getNome().equals("")) {
    				getResponse = response.split(split);
    				doc.setNome(getResponse[1]);
    			}else {
    				getResponse = response.split(split);
    				doc.setNome(doc.getNome() + " " + getResponse[1]);
    			}
    		}
    		if(response.contains("Rg")) {
    			if(doc.getRg() == null || doc.getRg().equals("")) {
    				getResponse = response.split(split);
    				getResponse = getResponse[1].split(split2);
    				doc.setRg(getResponse[0]);
    			}else {
    				getResponse = response.split(split);
    				doc.setRg(doc.getRg() + " " + getResponse[1]);
    			}
    		}
    		if(response.contains("Cpf")) {
    			if(doc.getCpf() == null && doc.getDataNascimento() == null || doc.getRg().equals("") && doc.getDataNascimento().equals("")) {
    				getResponse = response.split(split);
    				getResponse = getResponse[1].split(split2);
    				doc.setCpf(getResponse[0].trim().replaceAll("\\D", ""));
    				doc.setDataNascimento(getResponse[1]);
    			}else {
    				getResponse = response.split(split);
    				getResponse = getResponse[1].split(split2);
    				doc.setCpf(doc.getCpf() + "" + getResponse[0].trim().replaceAll("\\D", ""));
    				doc.setDataNascimento(doc.getDataNascimento() + " " + getResponse[1]);
    			}
    		}
    		if(response.contains("Pai")) {
    			if(doc.getPai() == null || doc.getPai().equals("")) {
    				getResponse = response.split(split);
    				doc.setPai(getResponse[1]);
    			}else {
    				getResponse = response.split(split);
    				doc.setPai(doc.getPai() + " " + getResponse[1]);
    			}
    		}
    		if(response.contains("Mae")) {
    			if(doc.getMae() == null || doc.getMae().equals("")) {
    				getResponse = response.split(split);
    				doc.setMae(getResponse[1]);
    			}else {
    				getResponse = response.split(split);
    				doc.setMae(doc.getMae() + " " + getResponse[1]);
    			}
    		}
    		if(response.contains("Categoria")) {
    			if(doc.getCategoria() == null || doc.getCategoria().equals("")) {
    				getResponse = response.split(split);
    				doc.setCategoria(getResponse[1]);
    			}
    		}
    		if(response.contains("Validade")) {
    			if(doc.getDataValidade() == null || doc.getDataValidade().equals("")) {
    				getResponse = response.split(split);
    				doc.setDataValidade(getResponse[1]);
    			}else {
    				getResponse = response.split(split);
    				doc.setDataValidade(doc.getDataValidade() + " " + getResponse[1]);
    			}
    		}
    	}
    	
        return doc;
    }
    
    public DocDTO getDocsRg(MultipartFile frente, MultipartFile verso) throws FileNotFoundException, IOException, ParseException {
    	analyzePhoto = new AnalyzePhotoService();
    	Calendar cal = Calendar.getInstance();
    	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    	
    	List<String> responseList = analyzePhoto.imageAnalyzeRg(verso.getBytes());
    	
    	DocDTO doc = new DocDTO();
    	String[] getResponse = null;
    	String split = ": ";
    	
    	for(String response : responseList) {
    		if(response.contains("Nome")) {
    			if(doc.getNome() == null || doc.getNome().equals("")) {
    				getResponse = response.split(split);
    				doc.setNome(getResponse[1]);
    			}else {
    				getResponse = response.split(split);
    				doc.setNome(doc.getNome() + " " + getResponse[1]);
    			}
    		}
    		if(response.contains("Rg")) {
    			if(doc.getRg() == null || doc.getRg().equals("")) {
    				getResponse = response.split(":");
    				getResponse = getResponse[1].split("REGISTRO ");
    				getResponse = getResponse[1].split("-");
    				doc.setRg(getResponse[0].trim().replaceAll("\\D", ""));
    				getResponse = getResponse[1].split(" DATA DE ");
    				doc.setRg(doc.getRg() + "" + getResponse[0]);
    				if(testDateFormat(getResponse[1])) {
						cal.setTime(new SimpleDateFormat("dd/MMM/yyyy").parse(getResponse[1]));     				
	    				doc.setDataExpedicao(sdf.format(cal.getTime()));
    				}else {
    					doc.setDataExpedicao(getResponse[1]);
    				}    				
    				if(doc.getRg().length() > 9) {
    					getResponse = doc.getRg().split(" ");
    					doc.setRg(getResponse[0]);
    				}
    			}else {
    				getResponse = response.split(split);
    				getResponse = getResponse[1].split("REGISTRO ");
    				getResponse = getResponse[1].split(" DATA DE");
    				doc.setRg(doc.getRg() + " " + getResponse[0].trim().replaceAll("\\D", ""));
    			}
    		}
    		if(response.contains("Data")) {
    			if(doc.getDataNascimento() == null || doc.getDataNascimento().equals("")) {
    				getResponse = response.split(split);
    				getResponse = getResponse[1].split("-");
    				getResponse = getResponse[1].split(" ");    				
    				if(testDateFormat(getResponse[1])) {
						cal.setTime(new SimpleDateFormat("dd/MMM/yyyy").parse(getResponse[1]));     				
	    				doc.setDataNascimento(sdf.format(cal.getTime()));
    				}else {
    					doc.setDataNascimento(getResponse[1]);
    				}    				
    			}else {
    				getResponse = response.split(split);
    				getResponse = getResponse[1].split("-");
    				getResponse = getResponse[1].split(" ");
    				doc.setDataNascimento(doc.getDataNascimento() + "" + getResponse[1]);
    			}
    		}
    		if(response.contains("Pai")) {
    			if(doc.getPai() == null || doc.getPai().equals("")) {
    				getResponse = response.split(split);
    				doc.setPai(getResponse[1]);
    			}else {
    				getResponse = response.split(split);
    				doc.setPai(doc.getPai() + " " + getResponse[1]);
    			}
    		}
    		if(response.contains("Mae")) {
    			if(doc.getMae() == null || doc.getMae().equals("")) {
    				getResponse = response.split(split);
    				doc.setMae(getResponse[1]);
    			}else {
    				getResponse = response.split(split);
    				doc.setMae(doc.getMae() + " " + getResponse[1]);
    			}
    		}
    		if(response.contains("Cpf")) {
    			if(doc.getCpf() == null || doc.getCpf().equals("")) {
    				getResponse = response.split(split);    				
    				if(getResponse[1].contains("CPF")) {
    					getResponse = getResponse[1].split(" ");
    					doc.setCpf(getResponse[1].trim().replaceAll("\\D", ""));
    					if(getResponse.length > 2) {
    						doc.setPis(getResponse[3].trim().replaceAll("\\D", ""));
    					}
    					if(doc.getCpf().length() > 11) {
    						doc.setCpf(doc.getCpf().substring(1,12));
    					}
    				}else {
	    				getResponse = getResponse[1].split(" ");
	    				doc.setCpf(getResponse[0].trim().replaceAll("\\D", ""));
    				}
    			}else {
    				getResponse = response.split(split);
    				getResponse = getResponse[1].split(" ");
    				doc.setCpf(doc.getCpf() + "" + getResponse[1].trim().replaceAll("\\D", ""));
    			}
    		}
    	}
    	
    	return doc;
    }
    
    public Boolean testDateFormat(String data) throws ParseException {
    	try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
			Date date = null;
			
			date = dateFormat.parse(data);
			
			if(date == null) {
				return false;
			}
			
			return true;
		} catch (ParseException e) {
			return false;
		}
    }
    
    public ResponseEntity<SsnPersonDTO> getDadosReceitaIni(String cpf, String birthDate) throws IOException{
    	for(int i=0; i<5; i++) {
    		ResponseEntity<SsnPersonDTO> response = getDadosReceita(cpf, birthDate);
    		if(response.getStatusCode() == HttpStatus.OK) {
    			return response;
    		}
    	}
    	
    	return ResponseEntity.badRequest().body(new SsnPersonDTO("-", "-", "Erro ao ler os dados, tente novamente mais tarde", "-", "-", "-"));
    }
    
    public ResponseEntity<SsnPersonDTO> getDadosReceita(String cpf, String birthDate) throws IOException {
    	AccessResponseDTO access = createRequest();
    	
    	String[] it = access.getCookie().substring(1, (access.getCookie().length() - 1)).split(",");
        Map<String, String> cookies = new HashMap<>();

        for (int i = 0; i < it.length; i++) {
            String[] res = it[i].split("=");
            cookies.put(res[0], res[1]);
        }
        
        byte[] captchaByte = DatatypeConverter.parseBase64Binary(access.getCaptchaBase64());
        
        PersonRequestDTO person = new PersonRequestDTO();
        person.setBirthDate(birthDate);
        person.setCpf(cpf);
        person.setCaptcha(getRecaptcha(captchaByte));
        	
    	return getPerson(person, cookies);
    }
    
    public String getRecaptcha(byte[] captcha) throws IOException {	
    	BufferedImage image = ImageIO.read(new ByteArrayInputStream(captcha));
    	image = resizeImage(image, 169, 51);
    	BufferedImage clean = cleanImage(image);
    	ImageIO.write(clean, "png",new File("D:\\Documentos\\Edmar\\React It\\DocsOcr\\clean.png"));
        return analyzePhoto.imageAnalyzeCaptcha(toByteArray(clean, "png"));
    }
    
    public static byte[] toByteArray(BufferedImage bi, String format) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bi, format, baos);
        byte[] bytes = baos.toByteArray();
        return bytes;
    }
    
    public static boolean isEligible(BufferedImage img, int x, int y){
        
        int left  = x;
        while (  left < 0 &&  x -left < 2* DELTA) {
            if( img.getRGB(left,y) == Color.WHITE.getRGB()) {
                break;
            }
            left --;
        }
        if( left < 0) {
            return false;
        }
        int right = x + 1;

        while ( right < img.getWidth() && right - left < 2 * DELTA) {
            if( img.getRGB(right,y) == Color.WHITE.getRGB()) {
                break;
            }
            right++;
        }
        if( right > img.getWidth()) {
            return false;
        }
        int top = y -1;
        while (top >0 && y - top < 2 * DELTA) {
            if( img.getRGB(x,top) == Color.WHITE.getRGB()) {
                break;
            }
            top --;
        }
        if( top < 0) {
            return false;
        }
        int bottom = y+1;
        while (bottom < img.getHeight() && bottom -top < 2* DELTA) {
            if( img.getRGB( x,bottom) == Color.WHITE.getRGB()) {
                break;
            }
            bottom++;
        }
        if( bottom > img.getHeight()) {
            return false;
        }


        int width = right -left;
        int height =  bottom - top;
        if( width >= DELTA && height >= DELTA) {
            return true;
        }
        return false;

    }

    public static BufferedImage cleanImage(BufferedImage source){
        BufferedImage clone = new BufferedImage(source.getWidth(),
                source.getHeight(), source.getType());
        Graphics2D g2d = clone.createGraphics();
        g2d.drawImage(source, 0, 0, null);
        g2d.dispose();
        for(int i=0;i<clone.getWidth();i++){
            for(int j=0;j<clone.getHeight();j++){
                int rgb = clone.getRGB(i,j);
                if( rgb == Color.WHITE.getRGB()){
                    continue;
                }
                if( isEligible(clone, i,j)) {
                    continue;
                }
                else {
                    clone.setRGB(i,j,Color.WHITE.getRGB());
                }

            }
        }

        return clone;
    }
    
    BufferedImage resizeImage(BufferedImage originalImage, int targetWidth, int targetHeight) throws IOException {
        Image resultingImage = originalImage.getScaledInstance(targetWidth, targetHeight, Image.SCALE_DEFAULT);
        BufferedImage outputImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_RGB);
        outputImage.getGraphics().drawImage(resultingImage, 0, 0, null);
        return outputImage;
    }
    
    public AccessResponseDTO createRequest() {
        /* Iremos criar uma conexão com a receita federal */
        Connection conn = Jsoup.connect("https://servicos.receita.fazenda.gov.br/Servicos/CPF/ConsultaSituacao/ConsultaPublicaSonoro.asp")
                /* Precisamos invalidar o certificado TLS para conseguir o acesso sem autenticar o protocolo*/
                .validateTLSCertificates(false)
                /* Devemos seguir os redirects da página */
                .followRedirects(true)
                /* Inserimos um timeout variável*/
                .timeout(1000)
                /* Vamos simular um UserAgent */
                .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36")
                .method(Connection.Method.GET);

        Connection.Response r = null;
        try {
            r = conn.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Document doc = Jsoup.parse(r.body());
        /*
        * Iremos capturar a imagem do captcha e retornar o base64 para que o
        * cliente possa responder o captcha, um passo muito importante é armazenar
        * os cookies em algum local, nesse caso estamos utilizando um banco de dados
        * H2 para que possamos buscar os cookies da sessão
        * */
        Element el = doc.getElementById("imgCaptcha");
        String base = el.attr("src");
        String bas = base.subSequence(base.indexOf(",") + 1, base.length()).toString();
        AccessResponseDTO acrs = new AccessResponseDTO();
        String uuid = UUID.randomUUID().toString();
        acrs.setCookie(r.cookies().toString());
        acrs.setCaptchaBase64(bas);
        acrs.setUuid(uuid);
        /* Vamos salvar a sessão dessa requisição para mais tarde utilizarmos os cookies*/
        return acrs;
    }
    
    public ResponseEntity<SsnPersonDTO> getPerson(PersonRequestDTO person, Map<String, String> cookies) {

        Document d = null;
        try {
            /* Efetuando um post com os dados criados e respondidos no request anterior */
            d = Jsoup.connect("https://servicos.receita.fazenda.gov.br/Servicos/CPF/ConsultaSituacao/ConsultaPublicaExibir.asp")
                    .data("idCheckedReCaptcha", "false")
                    .data("txtCPF", person.getCpf())
                    .data("txtDataNascimento", person.getBirthDate())
                    /* O captcha deve ser respondido */
                    .data("txtTexto_captcha_serpro_gov_br", person.getCaptcha())
                    .data("enviar", "Consultar")
                    .validateTLSCertificates(false)
                    .cookies(cookies)
                    .post();
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        }

        Document resposta = Jsoup.parse(d.body().toString());
        Elements els = resposta.getElementsByClass("clConteudoDados");
        Elements els2 = resposta.getElementsByClass("clConteudoComp");
        SsnPersonDTO pf = new SsnPersonDTO();
        
        pf.setNome(els.get(NAME_INDEX).text());
        pf.setDataNascimento(els.get(BIRTH_DATE_INDEX).text());
        pf.setSituacao(els.get(STATUS_INDEX).text());
        pf.setDataInscricao(els.get(DATE_INSCRICAO_INDEX).text());
        pf.setDigitoVerificado(els.get(DIGITO_VERIFICADOR_INDEX).text());
        pf.setCertificado(els2.get(0).text() + "  " + els2.get(1).text());

        return ResponseEntity.ok(pf);
    }
}
