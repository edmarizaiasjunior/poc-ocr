package com.reactit.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.reactit.service.dto.FaceComparisonDTO;

import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.rekognition.RekognitionClient;
import software.amazon.awssdk.services.rekognition.model.CompareFacesMatch;
import software.amazon.awssdk.services.rekognition.model.CompareFacesRequest;
import software.amazon.awssdk.services.rekognition.model.CompareFacesResponse;
import software.amazon.awssdk.services.rekognition.model.ComparedFace;
import software.amazon.awssdk.services.rekognition.model.DetectTextRequest;
import software.amazon.awssdk.services.rekognition.model.DetectTextResponse;
import software.amazon.awssdk.services.rekognition.model.Image;
import software.amazon.awssdk.services.rekognition.model.RekognitionException;
import software.amazon.awssdk.services.rekognition.model.TextDetection;

@Service
public class AnalyzePhotoService {
	
	private final Logger log = LoggerFactory.getLogger(AnalyzePhotoService.class);

	private Region region = Region.US_EAST_1;
	
	@Value("${ocr.aws.credential.secret-access-key:}")
	private String credentialSecretAccessKey;
	
	@Value("${ocr.aws.credential.access-key-id:}")
	private String credentialAccessKeyId;
	
	private AwsCredentials credentials = new AwsCredentials() {
		
		@Override
		public String secretAccessKey() {
			return credentialSecretAccessKey;
		}
		
		@Override
		public String accessKeyId() {
			return credentialAccessKeyId;
		}
	};
	
	private AwsCredentialsProvider provider = new AwsCredentialsProvider() {
		
		@Override
		public AwsCredentials resolveCredentials() {			
			return credentials;
		}
	};
	
	private RekognitionClient rekClient = RekognitionClient.builder().credentialsProvider(provider).region(region).build();
	
	public FaceComparisonDTO imageAnalyzeFaceComparison(byte[] source, byte[] target) throws IOException {
		FaceComparisonDTO faceComparison = new FaceComparisonDTO();
		Float similarityThreshold = 70F;
		
		try {	
	        SdkBytes sourceBytes = SdkBytes.fromByteArray(source);
	        SdkBytes targetBytes = SdkBytes.fromByteArray(target);
	        
            Image souImage = Image.builder()
                    .bytes(sourceBytes)
                    .build();

             Image tarImage = Image.builder()
                     .bytes(targetBytes)
                     .build();

             CompareFacesRequest facesRequest = CompareFacesRequest.builder()
                     .sourceImage(souImage)
                     .targetImage(tarImage)
                     .similarityThreshold(similarityThreshold)                     
                     .build();
             
             CompareFacesResponse compareFacesResult = rekClient.compareFaces(facesRequest);
             List<CompareFacesMatch> faceDetails = compareFacesResult.faceMatches();

             for (CompareFacesMatch match: faceDetails){
                 faceComparison.setPorcentagem(BigDecimal.valueOf(match.face().confidence()));
             }
             
             List<ComparedFace> uncompared = compareFacesResult.unmatchedFaces();
             if(uncompared.isEmpty()) {
            	 faceComparison.setCombina(true);
             }
             
             if(!uncompared.isEmpty()) {
	             log.info("There was " + uncompared.size() + " face(s) that did not match");
	             log.info("Source image rotation: " + compareFacesResult.sourceImageOrientationCorrection());
	             log.info("target image rotation: " + compareFacesResult.targetImageOrientationCorrection());
	             faceComparison.setCombina(false);
	             faceComparison.setPorcentagem(BigDecimal.ZERO);
	             return faceComparison;
             }
             
             return faceComparison;
			
		} catch (RekognitionException e) {
			log.error(e.getMessage());
		} 
		
		return faceComparison;
	}
	
	public String imageAnalyzeCaptcha(byte[] photo) throws IOException {	
		
		String response = "";
				
		try {
			SdkBytes sourceBytes = SdkBytes.fromByteArray(photo);
			
			Image imageForAnalyzing = Image.builder()
			        .bytes(sourceBytes)
			        .build();
			
			DetectTextRequest textRequest = DetectTextRequest.builder()
			        .image(imageForAnalyzing)
			       .build();
			
			DetectTextResponse textResponse = rekClient.detectText(textRequest);
			List<TextDetection> textCollection = textResponse.textDetections();
			for(TextDetection text : textCollection) {
				response += "Captcha: " + text.detectedText() + "\n";
			}
			
			String[] responseFinal = response.split(": ");
			
			return responseFinal[1];			
		} catch (RekognitionException e) {
			log.error(e.getMessage());
		} 
		
		return response;
	}
	
	public List<String> imageAnalyzeCnh(byte[] photo) throws IOException {	
		
		String response = "";
		List<String> responseTratamento = null;
				
		try {
			SdkBytes sourceBytes = SdkBytes.fromByteArray(photo);
			
			Image imageForAnalyzing = Image.builder()
			        .bytes(sourceBytes)
			        .build();
			
			DetectTextRequest textRequest = DetectTextRequest.builder()
			        .image(imageForAnalyzing)
			       .build();
			
			DetectTextResponse textResponse = rekClient.detectText(textRequest);
			List<TextDetection> textCollection = textResponse.textDetections();
			responseTratamento = new ArrayList<>();
			
			for(TextDetection text : textCollection) {
				if(testNameCnh(text.geometry().boundingBox().width(), text.geometry().boundingBox().height(), text.geometry().boundingBox().left(), text.geometry().boundingBox().top())) {
					response += "Nome: " + text.detectedText();
					responseTratamento.add(response);
					response = "";
				}				
				if(testRgCnh(text.geometry().boundingBox().width(), text.geometry().boundingBox().height(), text.geometry().boundingBox().left(), text.geometry().boundingBox().top())) {
					response += "Rg: " + text.detectedText();
					responseTratamento.add(response);
					response = "";
				}				
				if(testCpfAndDateCnh(text.geometry().boundingBox().width(), text.geometry().boundingBox().height(), text.geometry().boundingBox().left(), text.geometry().boundingBox().top())) {
					response += "Cpf e Data de Nascimento: " + text.detectedText();
					responseTratamento.add(response);
					response = "";
				}				
				if(testPaiCnh(text.geometry().boundingBox().width(), text.geometry().boundingBox().height(), text.geometry().boundingBox().left(), text.geometry().boundingBox().top())) {
					response += "Pai: " + text.detectedText();
					responseTratamento.add(response);
					response = "";
				}				
				if(testMaeCnh(text.geometry().boundingBox().width(), text.geometry().boundingBox().height(), text.geometry().boundingBox().left(), text.geometry().boundingBox().top())) {
					response += "Mae: " + text.detectedText();
					responseTratamento.add(response);
					response = "";
				}				
				if(testCategoriaCnh(text.geometry().boundingBox().width(), text.geometry().boundingBox().height(), text.geometry().boundingBox().left(), text.geometry().boundingBox().top())) {
					response += "Categoria: " + text.detectedText();
					responseTratamento.add(response);
					response = "";
				}				
				if(testValidadeCnh(text.geometry().boundingBox().width(), text.geometry().boundingBox().height(), text.geometry().boundingBox().left(), text.geometry().boundingBox().top())) {
					response += "Validade: " + text.detectedText();
					responseTratamento.add(response);
					response = "";
				}
			}
			
			return responseTratamento;
		} catch (RekognitionException e) {
			log.error(e.getMessage());
		} 
		
		return responseTratamento;
	}
	
	public List<String> imageAnalyzeRg(byte[] photo) throws IOException {
		
		String response = "";
		List<String> responseTratamento = null;
		
		try {
			SdkBytes sourceBytes = SdkBytes.fromByteArray(photo);
			
			Image imageForAnalyzing = Image.builder()
			        .bytes(sourceBytes)
			        .build();
			
			DetectTextRequest textRequest = DetectTextRequest.builder()
			        .image(imageForAnalyzing)
			       .build();
			
			DetectTextResponse textResponse = rekClient.detectText(textRequest);
			List<TextDetection> textCollection = textResponse.textDetections();
			responseTratamento = new ArrayList<>();
			
			for(TextDetection text : textCollection) {				
				if(testNameRg(text.geometry().boundingBox().width(), text.geometry().boundingBox().height(), text.geometry().boundingBox().left(), text.geometry().boundingBox().top())) {
					response += "Nome: " + text.detectedText();
					responseTratamento.add(response);
					response = "";
				}				
				if(testRgRg(text.geometry().boundingBox().width(), text.geometry().boundingBox().height(), text.geometry().boundingBox().left(), text.geometry().boundingBox().top())) {
					response += "Rg: " + text.detectedText();
					responseTratamento.add(response);
					response = "";
				}				
				if(testDateRg(text.geometry().boundingBox().width(), text.geometry().boundingBox().height(), text.geometry().boundingBox().left(), text.geometry().boundingBox().top())) {
					response += "Data de Nascimento: " + text.detectedText();
					responseTratamento.add(response);
					response = "";
				}				
				if(testPaiRg(text.geometry().boundingBox().width(), text.geometry().boundingBox().height(), text.geometry().boundingBox().left(), text.geometry().boundingBox().top())) {
					response += "Pai: " + text.detectedText();
					responseTratamento.add(response);
					response = "";
				}				
				if(testMaeRg(text.geometry().boundingBox().width(), text.geometry().boundingBox().height(), text.geometry().boundingBox().left(), text.geometry().boundingBox().top())) {
					response += "Mae: " + text.detectedText();
					responseTratamento.add(response);
					response = "";
				}				
				if(testCpfRg(text.geometry().boundingBox().width(), text.geometry().boundingBox().height(), text.geometry().boundingBox().left(), text.geometry().boundingBox().top())) {
					response += "Cpf: " + text.detectedText();
					responseTratamento.add(response);
					response = "";
				}
			}
			
			return responseTratamento;			
		} catch (RekognitionException e) {
			log.error(e.getMessage());
		} 
		
		return responseTratamento;
	}
	
	public Boolean testNameCnh(float width, float height, float left, float top) {		
		float widthMinor = 0.07879952F;
		float widthMajor = 0.44636875F;
		float heightMinor = 0.033388544F;
		float heightMajor = 0.04291057F;
		float leftMinor = 0.19975047F;
		float leftMajor = 0.29645616F;
		float topMinor = 0.24996345F;
		float topMajor = 0.26546586F;
		
		if(testWidth(widthMinor, widthMajor, width) && testHeight(heightMinor, heightMajor, height) && testLeft(leftMinor, leftMajor, left) && testTop(topMinor, topMajor, top)) {
			return true;
		}
		
		return false;
	}
	
	public Boolean testRgCnh(float width, float height, float left, float top) {		
		float widthMinor = 0.21121405F;
		float widthMajor = 0.26618266F;
		float heightMinor = 0.0344264F;
		float heightMajor = 0.03656556F;
		float leftMinor = 0.5145746F;
		float leftMajor = 0.53313035F;
		float topMinor = 0.3535521F;
		float topMajor = 0.36185652F;
		
		if(testWidth(widthMinor, widthMajor, width) && testHeight(heightMinor, heightMajor, height) && testLeft(leftMinor, leftMajor, left) && testTop(topMinor, topMajor, top)) {
			return true;
		}
		
		return false;
	}
	
	public Boolean testCpfAndDateCnh(float width, float height, float left, float top) {		
		float widthMinor = 0.41780812F;
		float widthMajor = 0.80285543F;
		float heightMinor = 0.04818196F;
		float heightMajor = 0.11078012F;
		float leftMinor = 0.11309244F;
		float leftMajor = 0.5053145F;
		float topMinor = 0.4358639F;
		float topMajor = 0.44260237F;
		
		if(testWidth(widthMinor, widthMajor, width) && testHeight(heightMinor, heightMajor, height) && testLeft(leftMinor, leftMajor, left) && testTop(topMinor, topMajor, top)) {
			return true;
		}
		
		return false;
	}
	
	public Boolean testPaiCnh(float width, float height, float left, float top) {		
		float widthMinor = 0.09582655F;
		float widthMajor = 0.3306969F;
		float heightMinor = 0.034303706F;
		float heightMajor = 0.04069891F;
		float leftMinor = 0.5227299F;
		float leftMajor = 0.634188F;
		float topMinor = 0.5434245F;
		float topMajor = 0.5598728F;
		
		if(testWidth(widthMinor, widthMajor, width) && testHeight(heightMinor, heightMajor, height) && testLeft(leftMinor, leftMajor, left) && testTop(topMinor, topMajor, top)) {
			return true;
		}
		
		return false;
	}
	
	public Boolean testMaeCnh(float width, float height, float left, float top) {		
		float widthMinor = 0.065159574F;
		float widthMajor = 0.34989932F;
		float heightMinor = 0.03353057F;
		float heightMajor = 0.03776546F;
		float leftMinor = 0.5190877F;
		float leftMajor = 0.7859042F;
		float topMinor = 0.6478964F;
		float topMajor = 0.7038505F;
		
		if(testWidth(widthMinor, widthMajor, width) && testHeight(heightMinor, heightMajor, height) && testLeft(leftMinor, leftMajor, left) && testTop(topMinor, topMajor, top)) {
			return true;
		}
		
		return false;
	}
	
	public Boolean testCategoriaCnh(float width, float height, float left, float top) {		
		float widthMinor = 0.038532111793756485F;
		float widthMajor = 0.03856382891535759F;
		float heightMinor = 0.02958579920232296F;
		float heightMajor = 0.030219780281186104F;
		float leftMinor = 0.8458715677261353F;
		float leftMajor = 0.8789893388748169F;
		float topMinor = 0.7928994297981262F;
		float topMajor = 0.807692289352417F;
		
		if(testWidth(widthMinor, widthMajor, width) && testHeight(heightMinor, heightMajor, height) && testLeft(leftMinor, leftMajor, left) && testTop(topMinor, topMajor, top)) {
			return true;
		}
		
		return false;
	}
	
	public Boolean testValidadeCnh(float width, float height, float left, float top) {		
		float widthMinor = 0.13430851697921753F;
		float widthMajor = 0.16880734264850616F;
		float heightMinor = 0.0374753437936306F;
		float heightMajor = 0.03846153989434242F;
		float leftMinor = 0.5064220428466797F;
		float leftMajor = 0.5186170339584351F;
		float topMinor = 0.8928571343421936F;
		float topMajor = 0.8994082808494568F;
		
		if(testWidth(widthMinor, widthMajor, width) && testHeight(heightMinor, heightMajor, height) && testLeft(leftMinor, leftMajor, left) && testTop(topMinor, topMajor, top)) {
			return true;
		}
		
		return false;
	}
	
	public Boolean testNameRg(float width, float height, float left, float top) {		
		float widthMinor = 0.5306463F;
		float widthMajor = 0.63568234F;
		float heightMinor = 0.045731917F;
		float heightMajor = 0.053988125F;
		float leftMinor = 0.10370507F;
		float leftMajor = 0.14828335F;
		float topMinor = 0.14294466F;
		float topMajor = 0.22527413F;
		
		if(testWidth(widthMinor, widthMajor, width) && testHeight(heightMinor, heightMajor, height) && testLeft(leftMinor, leftMajor, left) && testTop(topMinor, topMajor, top)) {
			return true;
		}
		
		return false;
	}
	
	public Boolean testRgRg(float width, float height, float left, float top) {		
		float widthMinor = 0.7554345F;
		float widthMajor = 0.8636693F;
		float heightMinor = 0.05275663F;
		float heightMajor = 0.059498325F;
		float leftMinor = 0.04715634F;
		float leftMajor = 0.085626684F;
		float topMinor = 0.06565361F;
		float topMajor = 0.081632465F;
		
		if(testWidth(widthMinor, widthMajor, width) && testHeight(heightMinor, heightMajor, height) && testLeft(leftMinor, leftMajor, left) && testTop(topMinor, topMajor, top)) {
			return true;
		}
		
		return false;
	}
	
	public Boolean testDateRg(float width, float height, float left, float top) {		
		float widthMinor = 0.803189F;
		float widthMajor = 0.8594978F;
		float heightMinor = 0.048901334F;
		float heightMajor = 0.052135024F;
		float leftMinor = 0.05138487F;
		float leftMajor = 0.102113485F;
		float topMinor = 0.49055648F;
		float topMajor = 0.5154508F;
		
		if(testWidth(widthMinor, widthMajor, width) && testHeight(heightMinor, heightMajor, height) && testLeft(leftMinor, leftMajor, left) && testTop(topMinor, topMajor, top)) {
			return true;
		}
		
		return false;
	}
	
	public Boolean testPaiRg(float width, float height, float left, float top) {		
		float widthMinor = 0.30078813F;
		float widthMajor = 0.7359475F;
		float heightMinor = 0.047115624F;
		float heightMajor = 0.050050847F;
		float leftMinor = 0.10207084F;
		float leftMajor = 0.15089492F;
		float topMinor = 0.25260982F;
		float topMajor = 0.33397385F;
		
		if(testWidth(widthMinor, widthMajor, width) && testHeight(heightMinor, heightMajor, height) && testLeft(leftMinor, leftMajor, left) && testTop(topMinor, topMajor, top)) {
			return true;
		}
		
		return false;
	}
	
	public Boolean testMaeRg(float width, float height, float left, float top) {		
		float widthMinor = 0.55288094F;
		float widthMajor = 0.6363886F;
		float heightMinor = 0.041122377F;
		float heightMajor = 0.050117422F;
		float leftMinor = 0.07375485F;
		float leftMajor = 0.10206127F;
		float topMinor = 0.36172798F;
		float topMajor = 0.39062285F;
		
		if(testWidth(widthMinor, widthMajor, width) && testHeight(heightMinor, heightMajor, height) && testLeft(leftMinor, leftMajor, left) && testTop(topMinor, topMajor, top)) {
			return true;
		}
		
		return false;
	}
	
	public Boolean testCpfRg(float width, float height, float left, float top) {		
		float widthMinor = 0.59315944F;
		float widthMajor = 0.8328017F;
		float heightMinor = 0.050996017F;
		float heightMajor = 0.093792684F;
		float leftMinor = 0.052296445F;
		float leftMajor = 0.10124499F;
		float topMinor = 0.7643594F;
		float topMajor = 0.8135882F;
		
		if(testWidth(widthMinor, widthMajor, width) && testHeight(heightMinor, heightMajor, height) && testLeft(leftMinor, leftMajor, left) && testTop(topMinor, topMajor, top)) {
			return true;
		}
		
		return false;
	}
	
	public Boolean testWidth(float minor, float major, float result) {
		if(result >= minor && result <= major) {
			return true;
		}
		
		return false;
	}
	
	public Boolean testHeight(float minor, float major, float result) {
		if(result >= minor && result <= major) {
			return true;
		}
		
		return false;
	}
	
	public Boolean testLeft(float minor, float major, float result) {
		if(result >= minor && result <= major) {
			return true;
		}
		
		return false;
	}
	
	public Boolean testTop(float minor, float major, float result) {
		if(result >= minor && result <= major) {
			return true;
		}
		
		return false;
	}
}
