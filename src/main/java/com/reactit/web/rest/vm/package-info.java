/**
 * View Models used by Spring MVC REST controllers.
 */
package com.reactit.web.rest.vm;
