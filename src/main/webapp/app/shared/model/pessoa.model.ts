import { Moment } from 'moment';

export interface IPessoa {
  id?: number;
  nome?: string;
  idade?: number;
  dataNascimento?: string;
}

export const defaultValue: Readonly<IPessoa> = {};
